require('./assets/styles/index.css');

require('angular');
require('angular-ui-router');
require('angular-ui-bootstrap');
require('./assets/styles/main.css');

angular.module('app',['ui.router','ui.bootstrap'])

.run(function(){
	console.log('Page is running!');
})

.config(function($stateProvider,$locationProvider,$urlRouterProvider){
	$locationProvider.html5Mode(true); // getting rid of # in Url
	$urlRouterProvider.otherwise('/'); // if unknown -> go to "/"

	$stateProvider

	.state("root",{
		url: '/', // what the url should look like
		component: 'root'
	});
})

// require("./components/root.component.js");
// require("./components/child.component.js");
// 
const context = require.context('.',true,/\.component\.js$/);
context.keys().forEach(context);
