angular.module('app')
  .component('curryModal', {
    template: `<div class="modal-body">
    <div>
      Name: Stephen Curry <br>
      Age: 29 <br>
      Championships: 2 <br>
      Team: Golden State Warriors
    </div>
    <button class="btn btn-warning" type="button" ng-click="$ctrl.handleClose()">Close Modal</button>
    </div>`,
    bindings: {
      modalInstance: "<",
      resolve: "<"
    },
    controller: [function() {
      var modal = this;

      modal.handleClose = function() {
        console.info("Closed Curry's modal");
        modal.modalInstance.close();
      };
    }]
  });