angular.module('app')


.component('secondPost',{
    template: 
            `
            <!--USER POST HEADER--> 
            <div class="post__header flex-between margin-16"> 
                <a class="profile profile--lg profile--hover-ul" href="#" ng-click=$ctrl.openLowry()> 
                    <img src="./assets/lowry.png"> 
                    <span>Kyle Lowry</span> 
                </a> 
                <span class="timestamp">Jun 9 at 5:23pm</span> 
            </div> 
            <!--USER POST CONTENT--> 
            <div class="text-md margin-16"> 
                Raptors are going to win next year!
            </div> 
            <!--USER POST FOOTER--> 
            <div class="flex-between margin-16"> 
                <div class="flex"> 
                    <a class="btn-icon btn--hover-red" href="#"><i class="fa fa-heart fa-lg"></i></a> 
                    <span id="likeNum">1</span> 
                </div> 
                <div class="flex">
                    <a class="btn-icon" href="#"><i class="fa fa-comments-o fa-lg"></i></a> 
                    <span>0</span> 
                </div> 
            </div> 
            `
    ,controller: function($uibModal) {
    var vm = this;
    vm.openLowry = function() {
        $uibModal.open({
        component: "lowryModal"});
      };
    vm.Like = function() {
        document.getElementById("likeButton").style='#FF0000';
        document.getElementById("likeNum").innerHTML++;
        
    };
    }

});





