angular.module('app')


.component('secondComment',{
	template: 
	`
	<ul class="margin-16 top-divider"> 
        <!-- POST INPUT COMPONENT--> 
        <div class="comments" > 
            <!--USER COMMENT-->
            <li class="flex-col"> 
                <!--USER COMMENT HEADER--> 
                <div class="flex-between"> 
                    <a class="profile profile--hover-ul" href="#" ng-click=$ctrl.openCurry()> 
                        <img src="./assets/westbrook.png"> 
                        <span>Russell Westbrook</span> 
                    </a> 
                    <span class="timestamp text-sm">Jun 9 at 6:02pm</span> 
                </div> 
                <!--USER COMMENT CONTENT-->
                <p class="text-md margin-8-top"> 
                    <img class="westbrookMeme" src="./assets/westbrookMeme.jpg">
                </p> 
            </li> 
        </div> 
        <div class="p-input"> 
            <div contentEditable="true" class="p-input__ta" placeholder="Add a comment..."></div> 
            <a class="btn btn-primary f-right margin-8-v" href="#">POST</a> 
        </div> 
    </ul>
    `

    ,controller: function($uibModal) {
    var vm = this;
    vm.openCurry = function() {
        $uibModal.open({
        component: "curryModal"});
      };
    }
   

});
