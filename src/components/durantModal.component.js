angular.module('app')
  .component('durantModal', {
    template: `<div class="modal-body">
    <div>
      Name: Kevin Durant <br>
      Age: 28 <br>
      Championships: 1 <br>
      Team: Golden State Warriors
    </div>
    <button class="btn btn-warning" type="button" ng-click="$ctrl.handleClose()">Close Modal</button>
    </div>`,
    bindings: {
      modalInstance: "<",
      resolve: "<"
    },
    controller: [function() {
      var modal = this;

      modal.handleClose = function() {
        console.info("Closed Durant's modal");
        modal.modalInstance.close();
      };
    }]
  });