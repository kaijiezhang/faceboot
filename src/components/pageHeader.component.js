angular.module('app')

.component('pageHeader',{
	template: 
        `
        <header class="page-header">
            <!--navbar block-->
            <div class="navbar">
                <a class="logo" href="http://facebook.com"></a>
                <ul class="nav-h">
                    <li>
                        <a class="btn profile profile--hover-bg" href="#" ng-click=$ctrl.openLebron()>
                            <img src="./assets/lebron.png">
                            <span>Lebron James</span>
                        </a>
                    </li>
                    <li>
                        <a class="btn" href="#" ng-click="$ctrl.goToGoogle()">
                            <i class="fa fa-sign-out" aria-hidden="true"></i>
                            <span>Log Out</span>
                        </a>
                    </li>
                </ul>
            </div>
        </header>
        `

    ,controller: function($uibModal) {
        var vm = this;
        vm.openLebron = function() {
            $uibModal.open({
            component: "lebronModal"});
        };
        vm.goToGoogle=function(){
            window.location.href='https://www.google.ca';
        };
    }
    
});

