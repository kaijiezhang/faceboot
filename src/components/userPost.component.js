angular.module('app')


.component('userPost',{
    template: 
    `
    <ul class="margin-16 top-divider"> 
        <div class="comments" > 
            <!--USER COMMENT-->
            <li class="flex-col" ng-repeat="comments in commentsArray"> 
                <!--USER COMMENT HEADER--> 
                <div class="flex-between"> 
                    <a class="profile profile--hover-ul" href="#" ng-click=$ctrl.openCurry()> 
                        <img src="{{comments.image}}"> 
                        <span>{{comments.name}}</span> 
                    </a> 
                    <span class="timestamp text-sm">{{comments.date}}</span> 
                </div> 
                <!--USER COMMENT CONTENT-->
                <p class="text-md margin-8-top"> 
                    {{comments.comment}}
                </p> 
            </li> 
        </div> 
        <!-- POST INPUT COMPONENT--> 
        <div class="p-input"> 
            <input contentEditable="true" class="p-input__ta" placeholder="Add a comment..." ng-model="commentContent"></input> 
            <a class="btn btn-primary f-right margin-8-v" href="#" ng-click=$ctrl.postComment()>POST</a> 
        </div> 
    </ul>
    `

    ,controller: function($uibModal, $scope) {
        
    var vm = this;
    $scope.commentsArray=[{name:"Stephen Curry", image: "./assets/curry.png", date:"Jun 12 at 11:13pm",comment:"You were just along for the ride..."},
    {name:"Russell Westbrook", image: "./assets/westbrook.png", date:"Jun 12 at 11:20pm", comment:'Snake'}];
    vm.openCurry = function() {
        $uibModal.open({
        component: "curryModal"});
      };

    vm.postComment=function(){
        var today = new Date();
        var monthNames = ["January", "February", "March", "April", "May", "June",
              "July", "August", "September", "October", "November", "December"
            ];
        $scope.commentsArray.push({name:"Lebron James", image:"./assets/lebron.png", 
            date:monthNames[today.getMonth()]+ " " + today.getDate() + " at " + today.getHours() + ":" + today.getMinutes(),
            comment:$scope.commentContent});
    }
    }
   
});
