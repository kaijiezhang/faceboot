angular.module('app')

.config(function($stateProvider){
	$stateProvider

	.state('common', {
        templateUrl: 'tpl.common.html',
        abstract: true,
      })
	
	.state('profilePage',{
		url: '/profilePage',
		component: 'another'
	});
})

.component('another',{
	template: "profilePage.html"
});