angular.module('app')

.component('root',{
	template: 
	`
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"> 
	<html>
	<head> 
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width,initial-scale=1"> 
	    <title>FaceBoot</title>
	    <script src="https://use.fontawesome.com/7a32e5b7f0.js"></script> 
	    <link href="https://fonts.googleapis.com/css?family=Kalam" rel="stylesheet"> 
	    <link rel="stylesheet" href="assets/styles/main.css" type="text/css"> 
	</head> 
	<body>
	<audio autoplay>
	  <source src="./assets/champions.mp3" type="audio/mpeg">
	</audio>
	<page-header></page-header> 
		<div class="container"> 
			<div class="content">
				<div class="post">
				<main-post></main-post>
				<user-post></user-post>
				</div>
				<div class="post">
				<second-post></second-post>
				<second-comment></second-comment>
				</div>
			</div>
			<div class="aside">
				<a id="asideDurant" class="btn profile profile--hover-bg" href="#" ng-click=$ctrl.openDurant()
					<span>Kevin Durant</span></a>
				<a id="asideCurry" class="btn profile profile--hover-bg" href="#" ng-click=$ctrl.openCurry()
					<span>Stephen Curry</span></a>
			</div> 
		</div> 
	<footer class="footer">Footer</footer> 
	</body>
	</html>
	`

	,controller: function($uibModal) {
    var vm = this;
    vm.openDurant = function() {
        $uibModal.open({
        component: "durantModal"});
      };
    vm.openCurry = function() {
        $uibModal.open({
        component: "curryModal"});
      };
    }


});


	