angular.module('app')
  .component('lowryModal', {
    template: `<div class="modal-body">
    <div>
      Name: Kyle Lowry <br>
      Age: 31 <br>
      Championships: 0 <br>
      Team: Toronto Raptors
    </div>
    <button class="btn btn-warning" type="button" ng-click="$ctrl.handleClose()">Close Modal</button>
    </div>`,
    bindings: {
      modalInstance: "<",
      resolve: "<"
    },
    controller: [function() {
      var modal = this;

      modal.handleClose = function() {
        console.info("Closed Lowry's modal");
        modal.modalInstance.close();
      };
    }]
  });