angular.module('app')


.component('mainPost',{
    template: 
            `
            <!--USER POST HEADER--> 
            <div class="post__header flex-between margin-16"> 
                <a class="profile profile--lg profile--hover-ul" href="#" ng-click=$ctrl.openDurant()> 
                    <img src="./assets/durant.png"> 
                    <span>Kevin Durant</span> 
                </a> 
                <span class="timestamp">Jun 12 at 11:02pm</span> 
            </div> 
            <!--USER POST CONTENT--> 
            <div class="text-md margin-16"> 
                Got my first ring today!
            </div> 
            <!--USER POST FOOTER--> 
            <div class="flex-between margin-16"> 
                <div class="flex" ng-click="$ctrl.Like(); "> 
                    <a id="likeButton" class="btn-icon btn--hover-red" href="#"><i class="fa fa-heart fa-lg"></i> </a> 
                    <span id="likeNum">1067</span> 
                </div> 
                <div class="flex">
                    <a class="btn-icon" href="#"><i class="fa fa-comments-o fa-lg"></i></a> 
                    <span>2</span> 
                </div> 
            </div> 
            `
    ,controller: function($uibModal) {
    var vm = this;
    vm.openDurant = function() {
        $uibModal.open({
        component: "durantModal"});
      };
    vm.Like = function() {
        document.getElementById("likeButton").style='#FF0000';
        document.getElementById("likeNum").innerHTML++;
        
      };
    }
    
});





