angular.module('app')
  .component('lebronModal', {
    template: `<div class="modal-body">
    <div>
      Name: Lebron James <br>
      Age: 32 <br>
      Championships: 3 <br>
      Team: Cleveland Cavaliers
    </div>
    <button class="btn btn-warning" type="button" ng-click="$ctrl.handleClose()">Close Modal</button>
    </div>`,
    bindings: {
      modalInstance: "<",
      resolve: "<"
    },
    controller: [function() {
      var modal = this;

      modal.handleClose = function() {
        console.info("Closed Lebron's modal");
        modal.modalInstance.close();
      };
    }]
  });